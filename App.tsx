import React from 'react';
import { Provider as PaperProvider, DarkTheme } from 'react-native-paper';
import MainScreen from './src/MainScreen'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export default function App() {
  return (
    <PaperProvider theme={theme}
      settings={{
        icon: props => <FontAwesome5 {...props} />,
      }}
    >
      <MainScreen/>
    </PaperProvider>
  );
}

const theme = {
  ...DarkTheme,
  colors: {
    ...DarkTheme.colors
  }
}