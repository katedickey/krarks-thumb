import React, { useState } from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native'
import { Appbar, TextInput, withTheme, useTheme, Button, Divider, Portal, Modal } from 'react-native-paper';
import { scale } from "react-native-size-matters";

function MainScreen(props: { theme: ReactNativePaper.Theme; }) {
  const { colors } = props.theme;
  
  const [ thumbsTxt, setThumbsTxt ] = useState("1");
  const [ thumbs, setThumbs ] = useState(1);
  const [ krarksTxt, setKrarksTxt ] = useState("");
  const [ krarks, setKrarks ] = useState(0);
  const [ desiredWinsTxt, setDesiredWinsTxt ] = useState("");
  const [ desiredWins, setDesiredWins ] = useState(0);
  const [ desiredLossesTxt, setDesiredLossesTxt ] = useState("");
  const [ desiredLosses, setDesiredLosses ] = useState(0);

  const [ wins, setWins ] = useState(0);
  const [ losses, setLosses ] = useState(0);

  const [ wait, setWait ] = useState(false);

  const onChangeKrarks = (value: string) => {
    let k = onChangeNumeric(setKrarksTxt, setKrarks, krarksTxt, value);

    setDesiredWins(k);
    setDesiredWinsTxt(k.toString());

    setDesiredLosses(0);
    setDesiredLossesTxt("0");
  }

  const onChangeDesired = (what: string, value: string) => {
    if (value == ""){
      switch(what){
        case 'wins':
          onChangeNumeric(setDesiredWinsTxt, setDesiredWins, desiredWinsTxt, value)
          return;
        case 'losses':
          onChangeNumeric(setDesiredLossesTxt, setDesiredLosses, desiredLossesTxt, value);
          return;
        default:
          return;
      }
    }

    let parsed: number = parseInt(value);
    if (parsed != null && !isNaN(parsed) && parsed >= 0 && parsed <= krarks)
    {
      let dw: number, dl: number;
      switch(what){
        case 'wins':
          dw = onChangeNumeric(setDesiredWinsTxt, setDesiredWins, desiredWinsTxt, value);
          dl = krarks - dw;
          break;
        case 'losses':
          dl = onChangeNumeric(setDesiredLossesTxt, setDesiredLosses, desiredLossesTxt, value);
          dw = krarks - dl;
          break;
        default:
          return;
      }

      setDesiredWins(dw);
      setDesiredWinsTxt(dw.toString());
      setDesiredLosses(dl);
      setDesiredLossesTxt(dl.toString());
    }
  }

  const onPressFlip = () => {
    let flipsPerKrark: number = Math.pow(2, thumbs);

    let w: number = 0;
    let dl: number = desiredLosses;

    setTimeout(() => {
      for(let i=0; i < krarks; i++){
        let heads=0;
        let tails=0;
        for (let j=0; j < flipsPerKrark; j++){
            if (Math.round(Math.random()) > 0){
                heads++;
            }else{
                tails++;
            }
        }
        if (dl > 0 && tails > 0){
            dl--;
        }else if (heads > 0){
            w++;
        }
      }

      setWins(w);
      setLosses(krarks - w);
      setWait(false);
    }, 0);

    setWait(true);
  }

  const isFlipDisabled = () => 
      wait
      || thumbsTxt === "" 
      || krarksTxt === "" 
      || desiredWinsTxt === ""
      || desiredLossesTxt === "";

  return (
    <>
      <Appbar.Header>
        <Appbar.Content title="Krark's Thumb"/>
      </Appbar.Header>
      <View style={{
        backgroundColor: '#000',
        flex: 1
      }}>
          <TextInput label="Thumbs" value={thumbsTxt} mode="outlined" keyboardType="numeric"
            onChangeText={onChangeNumeric.bind(null, setThumbsTxt, setThumbs, thumbsTxt)} 
            style={styles.textInput} theme={props.theme}/>
          <TextInput label="Krarks" value={krarksTxt} mode="outlined" keyboardType="numeric"
            onChangeText={onChangeKrarks}
            style={styles.textInput} theme={props.theme}/>
          <View style={{flexDirection: 'row'}}>
            <TextInput label="Desired Wins" value={desiredWinsTxt} mode="outlined" keyboardType="numeric" 
              onChangeText={onChangeDesired.bind(null, 'wins')}
              style={{...styles.textInput, flex: 1 }} theme={props.theme}/>
            <TextInput label="Desired Losses" value={desiredLossesTxt} mode="outlined" keyboardType="numeric" 
              onChangeText={onChangeDesired.bind(null, 'losses')}
              style={{...styles.textInput, flex: 1 }} theme={props.theme}/>
          </View>
          <Button disabled={isFlipDisabled()} mode="contained" 
            style={styles.flipButton} theme={props.theme} icon="coins" 
            onPress={onPressFlip}
          >
            Flip Coins
          </Button>
          <View style={{padding: 16}}>
            <Text style={{...styles.winLossText, color: colors.text}}>
              Wins: {wins}
            </Text>
            <Text style={{...styles.winLossText, color: colors.text}}>
              Losses: {losses}
            </Text>
          </View>
      </View>
      <Portal>
          <Modal visible={wait} contentContainerStyle={{...styles.waitModal, backgroundColor: colors.placeholder}}>
            <Text style={{color: colors.text}}>Flipping coins...</Text>
          </Modal>
      </Portal>
    </>
  );
}

function onChangeNumeric(
  txtSetter: React.Dispatch<React.SetStateAction<string>>, 
  setter: React.Dispatch<React.SetStateAction<number>>, 
  oldValue: string,
  value: string)
{
  if (value == ""){
    txtSetter("");
    setter(0);
    return 0;
  }
  let parsed: number = parseInt(value);
  if (parsed != null && !isNaN(parsed) && parsed >= 0)
  { 
    txtSetter(parsed.toString());
    setter(parsed);
    return parsed;
  }
  let oldParsed = parseInt(oldValue);
  if (oldParsed != null && !isNaN(oldParsed) && oldParsed > 0){
    return oldParsed;
  }
  return 0;
}

const styles = {

  textInput: {
    margin: 8
  },

  flipButton: {
    margin: 8,
    width: '50%',
    alignSelf: 'center'
  },

  winLossText: {
    margin: 8,
    fontSize: scale(24)
  },

  waitModal: {
    alignSelf: 'center',
    padding: 18
  }

}

export default withTheme(MainScreen);